using HarmonyPsaTextCostCalculator.BusinessLogic.Helpers;
using HarmonyPsaTextCostCalculator.BusinessLogic.Interfaces;
using HarmonyPsaTextCostCalculator.BusinessLogic.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Tests
{
    public class TextCalculatorTests
    {
        private Mock<IAccountDetails> AccountDetails;

        private ITotalCostCalculator TotalCostCalculator;

        private Guid CustomerGuid1;

        private IEnumerable<PriceBand> ExamplePriceBands =>
            new List<PriceBand>()
            {
                new PriceBand {
                    QtyFrom = 1,
                    QtyTo = 200,
                    PricePerTextMessage = 0.10m
                },
                new PriceBand {
                    QtyFrom = 201,
                    QtyTo = 500,
                    PricePerTextMessage = 0.08m
                },
                new PriceBand {
                    QtyFrom = 501,
                    QtyTo = 1000,
                    PricePerTextMessage = 0.06m
                },
                new PriceBand {
                    QtyFrom = 1001,
                    PricePerTextMessage = 0.03m
                },
            };

        [SetUp]
        public void Setup()
        {
            AccountDetails = new Mock<IAccountDetails>();
            CustomerGuid1 = new System.Guid("e0da66a1-2f78-45ac-a12e-553d33462edf");


            AccountDetails.Setup(x => 
                x.NumberOfTextMessagesSentInMonth(CustomerGuid1, 1, 2019)).Returns(700);
            AccountDetails.Setup(x =>
                x.NumberOfTextMessagesSentInMonth(CustomerGuid1, 2, 2019)).Returns(1000);
            AccountDetails.Setup(x =>
                x.NumberOfTextMessagesSentInMonth(CustomerGuid1, 3, 2019)).Returns(1100);
            AccountDetails.Setup(x =>
                x.NumberOfTextMessagesSentInMonth(CustomerGuid1, 4, 2019)).Returns(50);


            AccountDetails.Setup(x =>
                x.GetAccountPriceBands(CustomerGuid1)).Returns(ExamplePriceBands);


            TotalCostCalculator = new TotalCostCalculator(AccountDetails.Object);
        }

        [Test]
        [TestCase(1, 2019, 56.0)]
        [TestCase(2, 2019, 74.0)]
        [TestCase(3, 2019, 77.0)]
        [TestCase(4, 2019, 5.0)]
        public void CalculateCost_ShouldBeAccurate(int monthNo, int yearNo, decimal expectedResult)
        {
            var testCost = TotalCostCalculator.CalculateCost(CustomerGuid1, monthNo, yearNo);
            Assert.AreEqual(testCost, expectedResult);
        }

    }
}