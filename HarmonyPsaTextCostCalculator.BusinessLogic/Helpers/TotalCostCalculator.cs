﻿using HarmonyPsaTextCostCalculator.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HarmonyPsaTextCostCalculator.BusinessLogic.Helpers
{
    public class TotalCostCalculator : ITotalCostCalculator
    {
        private readonly IAccountDetails accountDetails;

        public TotalCostCalculator(IAccountDetails _accountDetails)
        {
            accountDetails = _accountDetails;
        }

        public decimal CalculateCost(Guid customerAccount, int monthNo, int yearNo)
        {
            var messageQty = accountDetails.NumberOfTextMessagesSentInMonth(customerAccount, monthNo, yearNo);
            var customerPriceBands = accountDetails.GetAccountPriceBands(customerAccount);
            //check if user exists

            decimal finalCost = 0;

            foreach (var band in customerPriceBands)
            {
                if (messageQty > band.QtyFrom)
                {
                    var qtyPricedAtThisBand = 0;
                    var inclusiveQtyFrom = band.QtyFrom - 1;
                    if (band.QtyTo.HasValue)
                    {
                        qtyPricedAtThisBand = Math.Min(band.QtyTo.Value - inclusiveQtyFrom, messageQty - inclusiveQtyFrom);
                    }
                    else
                    {
                        qtyPricedAtThisBand = messageQty - inclusiveQtyFrom;
                    }

                    var bandCost = qtyPricedAtThisBand * band.PricePerTextMessage;
                    finalCost += bandCost;
                }
            }

            return finalCost;
        }
    }
}
