﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HarmonyPsaTextCostCalculator.BusinessLogic.Models
{
    public class PriceBand
    {
        public int QtyFrom { get; set; }
        public int? QtyTo { get; set; }
        public decimal PricePerTextMessage { get; set; }
    }
}
